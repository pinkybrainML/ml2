#Imports
import os
import numpy as np
import nibabel as nib

def patches(img_path, spaces):
	img = nib.load(imagepath).dataobj
	img = img[30:140,30:180,20:140,:]

	tickx_list = np.round(np.linspace(0,img.shape[0],spaces+1))
	ticky_list = np.round(np.linspace(0,img.shape[1],spaces+1))
	tickz_list = np.round(np.linspace(0,img.shape[2],spaces+1))

	patch_list = []
	for x in range(len(tickx_list)-1):
		for y in range(len(ticky_list)-1):
			for z in range(len(tickz_list)-1):
				x_start = int(tickx_list[x])
				x_end = int(tickx_list[x+1])
				y_start = int(ticky_list[y])
				y_end = int(ticky_list[y+1])
				z_start = int(tickz_list[z])
				z_end = int(tickz_list[z+1])
				patch_list.append(img[x_start:x_end,
					y_start:y_end,
					z_start:z_end, 0])

	return patch_list


def WhiteGreyMatter(patch):
	# 300 - 1200
	thresholds = np.concatenate(([0],np.linspace(300,1200,10),[1600]))
	
	outputList = []
	histogram = np.histogram(patch, bins=thresholds)
	return histogram[0]


def preprocessing(imagepath, spaces):
	outputList_colors = []

	patch_list = patches(imagepath, spaces)

	for patch in patch_list:
		outputs = WhiteGreyMatter(patch)
		for output in outputs:
			outputList_colors.append(output)


	return outputList_colors


####################################
#         MAIN METHOD              #
####################################

spaceList=[3,5,7,9,10,12]

# Preprocess all the training data with different space sizes
for spaces in spaceList:
	print('')
	print('########### SPACE:', spaces, '###########')

	# Preprocess all the training data
	training_buckets=[]
	for i in range(1,279):
		imagepath = os.path.join('../data/set_train/', 'train_' + str(i) + '.nii')
		print('Processing training', imagepath)
		outputList_colors = preprocessing(imagepath, spaces)
		training_buckets.append(outputList_colors)

	# Convert it to an np array
	training_buckets = np.asarray(training_buckets)

	# Save the preprocessed data in csv
	np.savetxt('./preprocess/training_' + str(spaces) + '.csv', training_buckets, delimiter=',')

# Preprocess all the test data with different space sizes
for spaces in spaceList:
	print('')
	print('########### SPACE:', spaces, '###########')
	
	# preprocess our test data
	test = []
	for i in range(1, 139):
		imagepath = os.path.join('../data/set_test/', 'test_' + str(i) + '.nii')
		print('Processing test', imagepath)
		outputList = preprocessing(imagepath, spaces)
		test.append(outputList)

	# Save the preprocessed data in csv
	np.savetxt('./preprocess/test_'+ str(spaces) +'.csv', test, delimiter=',')