#Imports
import os
import numpy as np
import nibabel as nib
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn.decomposition import SparseCoder
from scipy import signal

targets = np.loadtxt("./targets.csv", delimiter=",")
training3 = np.loadtxt("./preprocess/training_3.csv",delimiter=",")
training5 = np.loadtxt("./preprocess/training_5.csv",delimiter=",")
training7 = np.loadtxt("./preprocess/training_7.csv",delimiter=",")
training9 = np.loadtxt("./preprocess/training_9.csv",delimiter=",")
training10 = np.loadtxt("./preprocess/training_10.csv",delimiter=",")
training12 = np.loadtxt("./preprocess/training_12.csv",delimiter=",")
test3 = np.loadtxt("./preprocess/test_3.csv",delimiter=",")
test5 = np.loadtxt("./preprocess/test_5.csv",delimiter=",")
test7= np.loadtxt("./preprocess/test_7.csv",delimiter=",")
test9 = np.loadtxt("./preprocess/test_9.csv",delimiter=",")
test10 = np.loadtxt("./preprocess/test_10.csv",delimiter=",")
test12= np.loadtxt("./preprocess/test_12.csv",delimiter=",")



training1 = np.hstack((training3,training7,training9))
training2 = np.hstack((training7,training10))
training3 = np.hstack((training5,training10,training12))
training4 = np.hstack((training3,training7,training10))
training5 = training5


test1 = np.hstack((test3,test7,test9))
test2 = np.hstack((test7,test10))
test3 = np.hstack((test5,test10,test12))
test4 = np.hstack((test3,test7,test10))
test5 = test5


from sklearn.ensemble import RandomForestClassifier

clf1 = RandomForestClassifier(n_estimators=500,max_depth=5,criterion='entropy',max_features='auto',n_jobs=-1,class_weight='balanced')
clf2 = RandomForestClassifier(n_estimators=200,max_depth=100,criterion='entropy',max_features='auto',n_jobs=-1,class_weight='balanced',min_impurity_split=0.1)
clf3 = RandomForestClassifier(n_estimators=200,max_depth=20,criterion='entropy',max_features='auto',n_jobs=-1,class_weight='balanced',min_impurity_split=1e-7)
clf4 = RandomForestClassifier(n_estimators=100,max_depth=20,criterion='entropy',max_features='auto',n_jobs=-1,class_weight='balanced',min_impurity_split=1e-7)
clf5 = RandomForestClassifier(n_estimators=200,max_depth=10,criterion='entropy',max_features='auto',n_jobs=-1,class_weight='balanced')



clf1.fit(training1, targets)
clf2.fit(training2, targets)
clf3.fit(training3, targets)
clf4.fit(training4, targets)
clf5.fit(training5, targets)

prediction1 = clf1.predict_proba(test1)[:,1]
prediction2 = clf2.predict_proba(test2)[:,1]
prediction3 = clf3.predict_proba(test3)[:,1]
prediction4 = clf4.predict_proba(test4)[:,1]
prediction5 = clf5.predict_proba(test5)[:,1]


prediction1 = np.asarray(prediction1).reshape(138,1)
prediction2 = np.asarray(prediction2).reshape(138,1)
prediction3 = np.asarray(prediction3).reshape(138,1)
prediction4 = np.asarray(prediction4).reshape(138,1)
prediction5 = np.asarray(prediction5).reshape(138,1)




index = np.asarray(range(1,139)).reshape(138,1)

output1 = np.hstack((index, prediction1))
output2= np.hstack((index, prediction2))
output3 = np.hstack((index, prediction3))
output4 = np.hstack((index, prediction4))
output5 = np.hstack((index, prediction5))


np.savetxt('./prediction/rfc.out', output1, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')

np.savetxt('./prediction/best_new5.out', output2, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')

np.savetxt('./prediction/best_new2.out', output3, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')

np.savetxt('./prediction/best_new.out', output4, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')

np.savetxt('./prediction/best.out', output5, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')
