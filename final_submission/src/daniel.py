#Imports
import os
import numpy as np
import nibabel as nibt
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
from sklearn.ensemble import RandomForestClassifier

# Multiload dimension 3,7,11
training3 = np.genfromtxt('./preprocess/training_3.csv', delimiter=',')
training7 = np.genfromtxt('./preprocess/training_7.csv', delimiter=',')
training11 = np.genfromtxt('./preprocess/training_9.csv', delimiter=',')

training = np.hstack((training3, training7, training11))

test3 = np.genfromtxt('./preprocess/test_3.csv', delimiter=',')
test7 = np.genfromtxt('./preprocess/test_7.csv', delimiter=',')
test11 = np.genfromtxt('./preprocess/test_9.csv', delimiter=',')

test = np.hstack((test3, test7, test11))
targets = np.genfromtxt('../data/targets.csv')

# Calculate the score
clf = RandomForestClassifier(n_estimators=500, max_depth=5, class_weight='balanced', criterion='entropy')
scores = cross_val_score(clf, training, targets, cv=10, scoring="neg_log_loss")
print("SCORE:", np.mean(scores))
print("STD:", np.std(scores))
print(scores)

# Fit and predict
clf.fit(training, targets)
prediction = clf.predict(test)


# Output the prediction
prediction = np.asarray(prediction).reshape(138,1)

index = np.asarray(range(1,139)).reshape(138,1)
output = np.hstack((index, prediction))
np.savetxt('./prediction/rfc.out', output, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')