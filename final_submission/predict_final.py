# This script produces the final submission as shown on
# the kaggle leaderboard.
#
# It runs correctly if placed next to the folders /src
# and /data. The folder /src contains whatever other
# scripts you need (provided by you). The folder /data
# can be assumed to contain targets.csv and two folders /set_train and
# /set_test which again contain the training and test
# samples respectively (provided by user, i.e. us).
#
# Its output is "final_sub.csv"


import numpy as np

# Get all the data
best1 = np.genfromtxt('src/prediction/best.out', delimiter=',', skip_header=True)
best2 = np.genfromtxt('src/prediction/best_new.out', delimiter=',', skip_header=True)
best3 = np.genfromtxt('src/prediction/best_new2.out', delimiter=',', skip_header=True)
best4 = np.genfromtxt('src/prediction/best_new5.out', delimiter=',', skip_header=True)
best5 = np.genfromtxt('src/prediction/rfc.out', delimiter=',', skip_header=True)

# Average the best submission and output as final_sub.out
prediction = np.mean((best1, best2, best3, best4, best5), axis=0)
np.savetxt('./final_sub.csv', prediction, delimiter=',', fmt='%i,%f', header='ID,Prediction', comments='')


